package pl.polak.easybadge;

public interface Builder<T> {

    T build();

}
