package pl.polak.easybadge.util;

import android.content.Context;

/**
 * Util converter class
 */
public final class AndroidConverterUtil {

    /**
     * Convert dip to pixels
     * @param dip
     * @return converted pixel value
     */
    public static int dip2Px(Context context, float dip) {
        return (int) (dip * context.getResources().getDisplayMetrics().density + 0.5f);
    }
}
