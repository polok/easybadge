package pl.polak.easybadge;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;

public interface DeafultParamProvider {

    static int DEFAULT_MARGIN = 0;
    static int DEFAULT_PADDING = 0;
    static int DEFAULT_TEXT_SIZE = 12;
    static int DEFAULT_COLOR = Color.WHITE;
    static int DEFAULT_GRAVITY = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
    static Typeface DEFAULT_TYPEFACE = Typeface.DEFAULT_BOLD;
}
