package pl.polak.easybadge;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.polak.easybadge.util.AndroidConverterUtil;

import static pl.polak.easybadge.DeafultParamProvider.*;

/**
 * Class responsible for adding TextView badge for a view
 */
public class EasyBadge extends TextView {

    private Context context;
    private Animation animation;

    private EasyBadge(ViewBuilder builder) {
        this(builder.targetView.getContext(), null);
        animation = builder.animation;

        setBadgeGravity(builder.gravity);
        setTargetView(builder.targetView);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, builder.textSize);
        setTextColor(builder.textColor);
        setTypeface(builder.typeface);
        setBackground(builder.radius, builder.backgroundColor);
        setBadgeMargin(builder);
        setBadgePaddings(builder);
    }

    private EasyBadge(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    private EasyBadge(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    private void setBadgeGravity(int badgeGravity) {
        if (!(getLayoutParams() instanceof LayoutParams)) {
            LayoutParams layoutParams =
                    new LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            badgeGravity);
            setLayoutParams(layoutParams);
        }
    }

    /**
     * Setup badge background color
     * @param dipRadius Badge radius
     * @param badgeColor Badge background color
     */
    @SuppressWarnings("deprecation")
    public void setBackground(int dipRadius, int badgeColor) {
        int radius = AndroidConverterUtil.dip2Px(context, dipRadius);
        float[] radiusArray = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };

        RoundRectShape roundRect = new RoundRectShape(radiusArray, null, null);
        ShapeDrawable bgDrawable = new ShapeDrawable(roundRect);
        bgDrawable.getPaint().setColor(badgeColor);
        setBackgroundDrawable(bgDrawable);
    }

    public void setBadgeText(String text) {
        setText(text);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (text == null || text.toString().trim().length() == 0) {
            setVisibility(View.GONE);
        } else {
            if(animation != null) {
                this.startAnimation(animation);
            }
            setVisibility(View.VISIBLE);
        }
        super.setText(text, type);
    }

    private void setBadgeMargin(ViewBuilder builder) {
        LayoutParams params = (LayoutParams) getLayoutParams();

        if(builder.badgeMargin != 0) {
            params.leftMargin = AndroidConverterUtil.dip2Px(context, builder.badgeMargin);
            params.topMargin = AndroidConverterUtil.dip2Px(context, builder.badgeMargin);
            params.rightMargin = AndroidConverterUtil.dip2Px(context, builder.badgeMargin);
            params.bottomMargin = AndroidConverterUtil.dip2Px(context, builder.badgeMargin);
        } else {
            params.leftMargin = AndroidConverterUtil.dip2Px(context, builder.badgeMarginLeft);
            params.topMargin = AndroidConverterUtil.dip2Px(context, builder.badgeMarginTop);
            params.rightMargin = AndroidConverterUtil.dip2Px(context, builder.badgeMarginRight);
            params.bottomMargin = AndroidConverterUtil.dip2Px(context, builder.badgeMarginBootom);
        }
        setLayoutParams(params);
    }

    private void setBadgePaddings(ViewBuilder builder) {
        if(builder.badgePadding != 0) {
            setPadding(AndroidConverterUtil.dip2Px(context, builder.badgePadding),
                    AndroidConverterUtil.dip2Px(context, builder.badgePadding),
                    AndroidConverterUtil.dip2Px(context, builder.badgePadding),
                    AndroidConverterUtil.dip2Px(context, builder.badgePadding));
        } else {
            setPadding(AndroidConverterUtil.dip2Px(context, builder.badgePaddingLeft),
                    AndroidConverterUtil.dip2Px(context, builder.badgePaddingTop),
                    AndroidConverterUtil.dip2Px(context, builder.badgePaddingRight),
                    AndroidConverterUtil.dip2Px(context, builder.badgePaddingBootom));
        }
    }

    private void setTargetView(View target) {
        if (target.getParent() instanceof FrameLayout) {
            ((FrameLayout) target.getParent()).addView(this);

        } else if (target.getParent() instanceof ViewGroup) {
            LinearLayout parentContainer = (LinearLayout) target.getParent();
            int groupIndex = parentContainer.indexOfChild(target);
            parentContainer.removeViewAt(groupIndex);

            FrameLayout badgeContainer = new FrameLayout(getContext());
            ViewGroup.LayoutParams parentlayoutParams = target.getLayoutParams();
            parentContainer.addView(badgeContainer, groupIndex, parentlayoutParams);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            badgeContainer.addView(target, params);
            badgeContainer.addView(this);
        }
    }

    public static class ViewBuilder implements Builder<EasyBadge> {
        private View targetView;
        private int backgroundColor;
        private Animation animation;
        private int gravity = DEFAULT_GRAVITY;
        private int radius = DEFAULT_TEXT_SIZE;
        private int textColor = DEFAULT_COLOR;
        private int textSize = DEFAULT_COLOR;
        private Typeface typeface = DEFAULT_TYPEFACE;
        private int badgeMargin = DEFAULT_MARGIN;
        private int badgeMarginTop = DEFAULT_MARGIN;
        private int badgeMarginBootom = DEFAULT_MARGIN;
        private int badgeMarginLeft = DEFAULT_MARGIN;
        private int badgeMarginRight = DEFAULT_MARGIN;
        private int badgePadding = DEFAULT_PADDING;
        private int badgePaddingTop = DEFAULT_PADDING;
        private int badgePaddingBootom = DEFAULT_PADDING;
        private int badgePaddingLeft = DEFAULT_PADDING;
        private int badgePaddingRight = DEFAULT_PADDING;

        public ViewBuilder(View targetView) {
            this.targetView = targetView;
        }

        public ViewBuilder gravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public ViewBuilder radius(int radius) {
            this.radius = radius;
            return this;
        }

        public ViewBuilder textColor(int textColor) {
            this.textColor = textColor;
            return this;
        }

        public ViewBuilder textSize(int textSize) {
            this.textSize = textSize;
            return this;
        }

        public ViewBuilder backgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        public ViewBuilder margin(int badgeMargin) {
            this.badgeMargin = badgeMargin;
            return this;
        }

        public ViewBuilder marginTop(int marginTop) {
            this.badgeMarginTop = marginTop;
            return this;
        }

        public ViewBuilder marginBottom(int marginBootom) {
            this.badgeMarginBootom = marginBootom;
            return this;
        }

        public ViewBuilder marginLeft(int marginLeft) {
            this.badgeMarginLeft = marginLeft;
            return this;
        }

        public ViewBuilder marginRight(int marginRight) {
            this.badgeMarginRight = marginRight;
            return this;
        }

        public ViewBuilder paddingTop(int paddingTop) {
            this.badgePaddingTop = paddingTop;
            return this;
        }

        public ViewBuilder paddingBottom(int paddingBootom) {
            this.badgePaddingBootom = paddingBootom;
            return this;
        }

        public ViewBuilder paddingLeft(int paddingLeft) {
            this.badgePaddingLeft = paddingLeft;
            return this;
        }

        public ViewBuilder paddingRight(int paddingRight) {
            this.badgePaddingRight = paddingRight;
            return this;
        }

        public ViewBuilder padding(int badgePadding) {
            this.badgePadding = badgePadding;
            return this;
        }

        public ViewBuilder typeFace(Typeface typeface) {
            this.typeface = typeface;
            return this;
        }

        public ViewBuilder animation(int animationResourceId) {
            this.animation = AnimationUtils.loadAnimation(targetView.getContext(), animationResourceId);
            return this;
        }

        @Override
        public EasyBadge build() {
            return new EasyBadge(this);
        }
    }

}