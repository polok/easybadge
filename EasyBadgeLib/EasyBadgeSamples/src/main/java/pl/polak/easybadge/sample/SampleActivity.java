package pl.polak.easybadge.sample;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import pl.polak.easybadge.EasyBadge;

public class SampleActivity extends Activity {

    private Button btnOne;
    private Button btnTwo;
    private Button btnThree;
    private ImageView ivAndroid;
    private ListView listView;

    private EasyBadge badgeViewThree;
    private static int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        initUiComponents();
        addBadges();
        addUiListeners();
    }

    private void addUiListeners() {
        btnThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                badgeViewThree.setBadgeText("" + ++counter);
            }
        });
    }

    private void initUiComponents() {
        btnOne = (Button) findViewById(R.id.btn_one);
        btnTwo = (Button) findViewById(R.id.btn_two);
        btnThree = (Button) findViewById(R.id.btn_three);
        ivAndroid = (ImageView) findViewById(R.id.iv_android);
    }

    private void addBadges() {
        EasyBadge badgeViewOne = new EasyBadge.ViewBuilder(btnOne)
                .margin(10)
                .padding(5)
                .backgroundColor(Color.BLACK)
                .build();
        badgeViewOne.setBadgeText("20");

        EasyBadge badgeViewTwo = new EasyBadge.ViewBuilder(btnTwo)
                .margin(8)
                .padding(2)
                .animation(R.anim.bounce)
                .backgroundColor(Color.RED)
                .gravity(Gravity.LEFT)
                .build();
        badgeViewTwo.setBadgeText("99");

        badgeViewThree = new EasyBadge.ViewBuilder(btnThree)
                .margin(8)
                .padding(2)
                .textSize(15)
                .typeFace(Typeface.SANS_SERIF)
                .backgroundColor(Color.BLUE)
                .animation(R.anim.fade_in)
                .gravity(Gravity.LEFT | Gravity.CENTER_VERTICAL)
                .build();

        EasyBadge badgeImage = new EasyBadge.ViewBuilder(ivAndroid)
                .gravity(Gravity.RIGHT)
                .animation(R.anim.blink)
                .marginLeft(45)
                .textSize(25)
                .textColor(Color.GREEN)
                .build();
        badgeImage.setBadgeText("I Robot");
    }

}
